#!/bin/bash
# bffa, Brian's Fabulous Firewall (for ARP)
# Copyright (C) 2015-2021 Brian Haslett
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


parse_config() {
  printf "\nBrian's Fabulous Firewall (ARP)\n"
   
  if [[ ! -e /etc/bff-nft.conf ]]; then
    printf "Error, config file not found!\n"
    exit 0
  fi

  source /etc/bff-nft.conf

  if [[ -n ${LANInterface} ]]; then
    if [[ "$ProcSysNet" -eq 1 ]]; then
      echo 2 > /proc/sys/net/ipv4/conf/${LANInterface}/arp_announce
      echo 3 > /proc/sys/net/ipv4/conf/${LANInterface}/arp_ignore
    else
      echo 1 > /proc/sys/net/ipv4/conf/${LANInterface}/arp_announce
      echo 2 > /proc/sys/net/ipv4/conf/${LANInterface}/arp_ignore
    fi
  fi

  echo "Configuration has been parsed."

}

rules_clear() {
  nft flush ruleset arp

  nft add table arp filter
  nft add chain arp filter INPUT '{ type filter hook input priority 0; policy accept; }'
  nft add chain arp filter OUTPUT '{ type filter hook output priority 0; policy accept; }'

}
create_filters() {
  local ARP_Misc="arp htype 1 arp hlen 6 arp plen 4"
  local ARP_Request="${ARP_Misc} arp operation request"
  local ARP_Reply="${ARP_Misc} arp operation reply"

  nft add chain arp filter INPUT '{ type filter hook input priority 0; policy drop; }'
  if [[ "$Filter_Outgoing" -eq 1 ]]; then 
    nft add chain arp filter OUTPUT '{ type filter hook output priority 0; policy drop; }'
  fi

  if [[ -n ${LANInterface} ]]; then
    for lan_mac in ${LAN_MACS[@]}; do
      nft add rule arp filter INPUT iifname ${LANInterface} ${ARP_Request} ether saddr ${lan_mac} counter accept
      nft add rule arp filter INPUT iifname ${LANInterface} ${ARP_Reply} ether saddr ${lan_mac} counter accept
    done
  fi
  for wan_mac in ${WAN_MACS[@]}; do
    nft add rule arp filter INPUT iifname ${WANInterface} ${ARP_Request} ether saddr ${wan_mac} counter accept
    nft add rule arp filter INPUT iifname ${WANInterface} ${ARP_Reply} ether saddr ${wan_mac} counter accept
  done

  if [[ $DockerSupport -eq 1 && -n ${BRIDGE_MAC} ]]; then
    nft add rule arp filter INPUT iifname ${DockerInterface} ${ARP_Request} arp saddr ip 172.17.0.1/16 ether saddr ${BRIDGE_MAC} counter accept
    nft add rule arp filter INPUT iifname ${DockerInterface} ${ARP_Reply} arp saddr ip 172.17.0.1/16 ether saddr ${BRIDGE_MAC} counter accept
  fi
  if [[ "$P2PWifiSupport" -eq 1 || -n ${LANInterface} ]]; then
    nft add rule arp filter INPUT iifname ${LANInterface} ${ARP_Misc} counter accept
    if [[ "$Filter_Outgoing" -eq 1 ]]; then 
      nft add rule arp filter OUTPUT oifname ${LANInterface} ${ARP_Misc} counter accept
    fi
  fi

  nft add rule arp filter INPUT iifname ${WANInterface} ${ARP_Misc} counter drop

  if [[ "$Filter_Outgoing" -eq 1 ]]; then 
    if [[ $DockerSupport -eq 1 && -n ${BRIDGE_MAC} ]]; then
      nft add rule arp filter OUTPUT oifname ${DockerInterface} ${ARP_Request} counter accept
      nft add rule arp filter OUTPUT oifname ${DockerInterface} ${ARP_Reply} counter accept
    fi
    nft add rule arp filter OUTPUT oifname ${WANInterface} ${ARP_Request} counter accept
    nft add rule arp filter OUTPUT oifname ${WANInterface} ${ARP_Reply} counter accept
  fi

  if [[ "$Log" -eq 1 ]]; then
    nft add rule arp filter INPUT ${ARP_Misc} counter log prefix \"nft arp, other in: \" level ${LogLev} flags all
    if [[ "$Filter_Outgoing" -eq 1 ]]; then
      nft add rule arp filter OUTPUT ${ARP_Misc} counter log prefix \"nft arp, other out: \" level ${LogLev} flags all
    fi
  fi
  echo "ARP packet filtering is now in place."
}

printf "Brian's Fabulous Firewall (for ARP)\n"
printf "^^^^^^^^^^^^^^^^^^^^^^^^^\n\n"
sleep 1

boolean() {
  local variable="$1"
  local value="$2"
  [[ "$value" == "on" ]] && value=1
  [[ "$value" == "off" ]] && value=0
  [[ "$value" == "true" ]] && value=1
  [[ "$value" == "false" ]] && value=0
  [[ "$value" == "ON" ]] && value=1
  [[ "$value" == "OFF" ]] && value=0
  [[ "$value" == "TRUE" ]] && value=1
  [[ "$value" == "FALSE" ]] && value=0
  [[ "$value" -gt 0 ]] && value=1 || value=0
  export "$variable"="$value"
}

[[ "$1" ]] && boolean userinput "$1" || boolean userinput on

start()
{
  rules_clear
  parse_config
  create_filters
}
stop()
{
  rules_clear
  echo "arptables disabled"
}

if [[ -z "$1" ]]; then
  printf "bffa (Brian's Fabulous Firewall, ARP edition)\n\n"
  printf "Usage:  bffa [on/start | off/stop | restart]\n"
  exit
else
  userinput="$1"
fi

if [[ "$userinput" == "off"  || "$userinput" == "stop" ]]; then
  stop
elif [[ "$userinput" == "on"  || "$userinput" == "start" ]]; then
  start
elif [[ "$userinput" == "restart" ]]; then
  stop
  start
fi

# vim: set syntax=sh et:
