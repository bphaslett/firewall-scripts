#!/bin/bash
#
# I use this to sanitize the config before pushing a commit
sed -e 's/boolean Unsolicited_TCP_LAN.*/boolean Unsolicited_TCP_LAN off/g' -i bff.conf
sed -e 's/boolean Unsolicited_UDP_LAN.*/boolean Unsolicited_UDP_LAN off/g' -i bff.conf
sed -e 's/boolean Unsolicited_TCP_WAN.*/boolean Unsolicited_TCP_WAN off/g' -i bff.conf
sed -e 's/boolean Unsolicited_UDP_WAN.*/boolean Unsolicited_UDP_WAN off/g' -i bff.conf
sed -e 's/boolean P2PWifiSupport.*/boolean P2PWifiSupport off/g' -i bff.conf
sed -e 's/boolean Allow_IGMP.*/boolean Allow_IGMP false/g' -i bff.conf
sed -e 's/boolean Allow_mDNS.*/boolean Allow_IGMP false/g' -i bff.conf

sed -e 's/LAN_MAC1=.*/LAN_MAC1\=[MAC ADDRESS]/g' -i bff.conf
sed -e 's/.*LAN_MAC2=.*/\#LAN_MAC2\=[MAC ADDRESS]/g' -i bff.conf
sed -e 's/.*BRIDGE_MAC=.*/\#BRIDGE_MAC\=[MAC ADDRESS]/g' -i bff.conf
sed -e 's/.*BRIDGE_SUBNET=.*/\#BRIDGE_SUBNET\=[BRIDGE SUBNET]/g' -i bff.conf
sed -e 's/LAN_v4SUBNET=.*/LAN_v4SUBNET\=[LAN IPv4 SUBNET]/g' -i bff.conf
sed -e 's/LAN_v4IP=.*/LAN_v4IP\=[LAN IPv4 GATEWAY]/g' -i bff.conf
sed -e 's/LAN_v6SUBNET=.*/LAN_v6SUBNET\=[LAN IPv6 SUBNET]/g' -i bff.conf
sed -e 's/LAN_v6IP=.*/LAN_v6IP\=[LAN IPv6 GATEWAY]/g' -i bff.conf
sed -e 's/LANInterface=.*/LANInterface\=[Network Interface]/g' -i bff.conf
sed -e 's/WANInterface=.*/WANInterface\=[Network Interface]/g' -i bff.conf
sed -e 's/.*DockerSupport.*/boolean DockerSupport false/g' -i bff.conf
sed -e 's/.*ProcSysNet.*/boolean ProcSysNet false/g' -i bff.conf

sed -e 's/boolean Unsolicited_TCP_LAN.*/boolean Unsolicited_TCP_LAN off/g' -i bff-nft.conf
sed -e 's/boolean Unsolicited_UDP_LAN.*/boolean Unsolicited_UDP_LAN off/g' -i bff-nft.conf
sed -e 's/boolean Unsolicited_TCP_WAN.*/boolean Unsolicited_TCP_WAN off/g' -i bff-nft.conf
sed -e 's/boolean Unsolicited_UDP_WAN.*/boolean Unsolicited_UDP_WAN off/g' -i bff-nft.conf
sed -e 's/boolean P2PWifiSupport.*/boolean P2PWifiSupport off/g' -i bff-nft.conf
sed -e 's/boolean Allow_IGMP.*/boolean Allow_IGMP false/g' -i bff-nft.conf
sed -e 's/boolean Allow_mDNS.*/boolean Allow_IGMP false/g' -i bff-nft.conf

sed -e 's/WAN_MACS=.*/WAN_MACS\=\"[MAC ADDRESSES]\"/g' -i bff-nft.conf
sed -e 's/.*LAN_MACS=.*/\#LAN_MACS\=\"[MAC ADDRESSES]\"/g' -i bff-nft.conf
sed -e 's/.*BRIDGE_MAC=.*/\#BRIDGE_MAC\=[MAC ADDRESS]/g' -i bff-nft.conf
sed -e 's/.*BRIDGE_SUBNET=.*/\#BRIDGE_SUBNET\=[BRIDGE SUBNET]/g' -i bff-nft.conf
sed -e 's/LAN_v4SUBNET=.*/LAN_v4SUBNET\=[LAN IPv4 SUBNET]/g' -i bff-nft.conf
sed -e 's/LAN_v4IP=.*/LAN_v4IP\=[LAN IPv4 GATEWAY]/g' -i bff-nft.conf
sed -e 's/LAN_v6SUBNET=.*/LAN_v6SUBNET\=[LAN IPv6 SUBNET]/g' -i bff-nft.conf
sed -e 's/LAN_v6IP=.*/LAN_v6IP\=[LAN IPv6 GATEWAY]/g' -i bff-nft.conf
sed -e 's/LANInterface=.*/LANInterface\=[Network Interface]/g' -i bff-nft.conf
sed -e 's/WANInterface=.*/WANInterface\=[Network Interface]/g' -i bff-nft.conf
sed -e 's/.*DockerSupport.*/boolean DockerSupport false/g' -i bff-nft.conf
sed -e 's/.*ProcSysNet.*/boolean ProcSysNet false/g' -i bff-nft.conf
