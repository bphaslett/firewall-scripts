#!/bin/bash
#
# bff configurator
# a work in progress..

# make sure the script can run
declare -a requires=(ip grep awk sed route cut wc head)
for f in $(echo ${requires[*]}); do
  if [[ -z `which $f` ]]; then
    printf "missing required file: $f\n"
    exit
  fi
done

WANInterface=$(ip route | grep default | awk '{print $5}')

if [[ -n $WANInterface ]]; then
  printf "WANInterface=%s\n" $WANInterface
else
  printf "Unable to determine WANInterface\n"
  exit
fi

LAN_v4IP=$(route -n | grep $WANInterface | grep UG | awk '{print $2}')

if [[ -n $LAN_v4IP ]]; then
  printf "LAN_v4IP=%s\n" $LAN_v4IP
else
  printf "Unable to determine LAN_v4IP\n"
  exit
fi


LAN_MAC1=$(ip -4 neigh | grep $WANInterface | grep $LAN_v4IP | awk '{print $5}')
printf "LAN_MAC1=%s\n" $LAN_MAC1
if [[ -n $(which docker) ]]; then
  BRIDGE_MAC=$(docker network inspect bridge | grep MacAddress | awk '{print $2}' | cut -d\" -f2)
  BRIDGE_SUBNET=$(docker network inspect bridge | grep Subnet | awk '{print $2}' | cut -d\" -f2)
  if [[ -n $BRIDGE_MAC ]]; then
    printf "BRIDGE_MAC=%s\n" $BRIDGE_MAC
    printf "BRIDGE_SUBNET=%s\n" $BRIDGE_SUBNET
  else
    printf "docker found, but unable to determine BRIDGE_MAC (maybe you aren't running any containers?)\n"
  fi
fi


zero_count=$(route -n | grep $WANInterface | grep -v UG | grep -v -e '^'"$LAN_v4IP" | awk '{print $3}' | sed -e 's/\./\n/g' | grep 0 | wc -l)
let bit_count=8*$zero_count;
let prefix_len=32-$bit_count;
LAN_v4SUBNET=$(route -n | grep $WANInterface | grep -v UG | grep -v UH | awk '{print $1}' | grep -v 169)/${prefix_len};

if [[ -n $LAN_v4SUBNET ]]; then
  printf "LAN_v4SUBNET=%s\n" $LAN_v4SUBNET
else
  printf "Unable to determine LAN_v4SUBNET\n"
fi

