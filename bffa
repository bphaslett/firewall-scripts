#!/bin/bash
# bffa, Brian's Fabulous Firewall (for ARP)
# Copyright (C) 2015-2021 Brian Haslett
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


parse_config() {
  printf "\nBrian's Fabulous Firewall (ARP)\n"
   
  if [[ ! -e /etc/bff.conf ]]; then
    printf "Error, config file not found!\n"
    exit 0
  fi

  source /etc/bff.conf

	if [[ -n ${LANInterface} ]]; then
		if [[ "$ProcSysNet" -eq 1 ]]; then
			echo 2 > /proc/sys/net/ipv4/conf/${LANInterface}/arp_announce
			echo 3 > /proc/sys/net/ipv4/conf/${LANInterface}/arp_ignore
		else
			echo 1 > /proc/sys/net/ipv4/conf/${LANInterface}/arp_announce
			echo 2 > /proc/sys/net/ipv4/conf/${LANInterface}/arp_ignore
		fi
	fi
	if [[ "$ProcSysNet" -eq 1 ]]; then
		echo 2 > /proc/sys/net/ipv4/conf/${WANInterface}/arp_announce
		echo 3 > /proc/sys/net/ipv4/conf/${WANInterface}/arp_ignore
	else
		echo 1 > /proc/sys/net/ipv4/conf/${WANInterface}/arp_announce
		echo 2 > /proc/sys/net/ipv4/conf/${WANInterface}/arp_ignore
	fi

  echo "Configuration has been parsed."

}

create_filters() {
  arptables -F
  arptables -t raw -F
  arptables -X 
  arptables -t raw -X
  arptables -Z

  arptables -P INPUT DROP
  if [[ "$Filter_Outgoing" -eq 1 ]]; then 
    arptables -P OUTPUT DROP 
  else
    arptables -P OUTPUT ACCEPT
  fi

  arptables -A INPUT -i ${WANInterface} -s ${LAN_v4IP} --source-mac ${LAN_MAC1} --opcode Request -j ACCEPT
  arptables -A INPUT -i ${WANInterface} -s ${LAN_v4IP} --source-mac ${LAN_MAC1} --opcode Reply -j ACCEPT
  if [[ -n ${LAN_MAC2} ]]; then
    arptables -A INPUT -i ${LANInterface} --source-mac ${LAN_MAC2} --opcode Request -j ACCEPT
    arptables -A INPUT -i ${LANInterface} --source-mac ${LAN_MAC2} --opcode Reply -j ACCEPT
  fi
  if [[ $DockerSupport -eq 1 && -n ${BRIDGE_MAC} ]]; then
    arptables -A INPUT -i ${DockerInterface} -s 172.17.0.01/16 --source-mac ${BRIDGE_MAC} --opcode Request -j ACCEPT
    arptables -A INPUT -i ${DockerInterface} -s 172.17.0.01/16 --source-mac ${BRIDGE_MAC} --opcode Reply -j ACCEPT
  fi
  if [[ "$P2PWifiSupport" -eq 1 || -n ${LANInterface} ]]; then
    arptables $VerbF -A INPUT -i $LANInterface -j ACCEPT
    arptables $VerbF -A OUTPUT -o $LANInterface -j ACCEPT
  fi

  arptables -A INPUT -i ${WANInterface} -s ${LAN_v4SUBNET} -j DROP

  if [[ "$Filter_Outgoing" -eq 1 ]]; then 
    if [[ $DockerSupport -eq 1 && -n ${BRIDGE_MAC} ]]; then
      arptables -A OUTPUT -o ${DockerInterface} --opcode Request -j ACCEPT
      arptables -A OUTPUT -o ${DockerInterface} --opcode Reply -j ACCEPT
    fi
    arptables -A OUTPUT -o ${WANInterface} --opcode Request -j ACCEPT
    arptables -A OUTPUT -o ${WANInterface} --opcode Reply -j ACCEPT
  fi

  echo "ARP packet filtering is now in place."
}

printf "Brian's Fabulous Firewall (for ARP)\n"
printf "^^^^^^^^^^^^^^^^^^^^^^^^^\n\n"
sleep 1

boolean() {
  local variable="$1"
  local value="$2"
  [[ "$value" == "on" ]] && value=1
  [[ "$value" == "off" ]] && value=0
  [[ "$value" == "true" ]] && value=1
  [[ "$value" == "false" ]] && value=0
  [[ "$value" == "ON" ]] && value=1
  [[ "$value" == "OFF" ]] && value=0
  [[ "$value" == "TRUE" ]] && value=1
  [[ "$value" == "FALSE" ]] && value=0
  [[ "$value" -gt 0 ]] && value=1 || value=0
  export "$variable"="$value"
}

[[ "$1" ]] && boolean userinput "$1" || boolean userinput on

start()
{
  parse_config
  create_filters
}
stop()
{
  arptables -F
  arptables -P INPUT ACCEPT
  arptables -P OUTPUT ACCEPT 
  echo "arptables disabled"
}

if [[ -z "$1" ]]; then
  printf "bffa (Brian's Fabulous Firewall, ARP edition)\n\n"
  printf "Usage:  bffa [on/start | off/stop | restart]\n"
  exit
else
  userinput="$1"
fi

if [[ "$userinput" == "off"  || "$userinput" == "stop" ]]; then
  stop
elif [[ "$userinput" == "on"  || "$userinput" == "start" ]]; then
  start
elif [[ "$userinput" == "restart" ]]; then
  stop
  start
fi

# vim: set syntax=sh et:
